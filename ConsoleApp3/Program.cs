﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            NodeList<Employee> employeeNodes = new NodeList<Employee>(); 
            Customer cus = new Customer("name");
            NodeList2<Customer> customerNodes = new NodeList2<Customer>();
            enumratedobj<Rainbow> rain = new enumratedobj<Rainbow>();
            Interfaceconstrain<Employee> emp = new Interfaceconstrain<Employee>();
            BaseClassconstrain<Employee> emp2 = new BaseClassconstrain<Employee>();
            TwoArgumentConstrain<Employee, IEmployee> emp3 = new TwoArgumentConstrain<Employee, IEmployee>();
            customerNodes.add();
            employeeNodes.add();
        }
    }
    public class NodeList<T> where T : new()
    {
        public T add()
        {
            return new T();
        }
    }
    public class NodeList2<T> where T : class
    {
        public void add()
        {
            
        }
    }
    public class enumratedobj<T> where T:System.Enum
    {
        public object values = Enum.GetValues(typeof(T));
        
    }
    public class Interfaceconstrain<T> where T :IEmployee
    {

    }
    public class BaseClassconstrain<T> where T : Employee
    {

    }
    public class TwoArgumentConstrain<T , U> where T : U
    {
        public void DoWork(T subClass, U baseClass)
        {

        }
    }
    public class Employee :IEmployee
    {
        public Employee()
        {

        }
    }

    public class Customer :Employee
    {
        public string name;
        public Customer(string customerName)
        {
            name = customerName;
            Console.WriteLine("hiiiiiii" + customerName);
        }
    }
    enum Rainbow
    {
        Red,
        Orange,
        Yellow,
        Green,
        Blue,
        Indigo,
        Violet
    }
    public interface IEmployee
    {
    }
}
